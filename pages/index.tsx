import Head from 'next/head'
import { getSession } from 'next-auth/react'
import { Session } from 'next-auth'

import Header from '../components/Header'
import Login from '../components/Login'
import Widgets from '../components/Widgets'

import Sidebar from '../components/Sidebar'
import Feed from '../components/Feed'
import { GetServerSideProps } from 'next'

import { collection, getDocs, orderBy, query } from 'firebase/firestore'
import { db } from '../firebase'

interface IProps {
  session: Session
  posts: any
}
export default function Home({ session, posts }: IProps) {
  if (!session) return <Login />

  return (
    <div className="h-screen overflow-hidden bg-gray-100">
      <Head>
        <title>Facebook</title>
      </Head>

      <Header />

      <main className="flex">
        <Sidebar />
        <Feed posts={posts} />
        <Widgets />
      </main>
    </div>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context)

  const q = query(collection(db, 'posts'), orderBy('timestamp', 'desc'))

  const postDocs = await getDocs(q)
  const posts = postDocs.docs.map((post) => ({
    id: post.id,
    ...post.data(),
    timestamp: null,
  }))

  return {
    props: {
      session,
      posts,
    },
  }
}
