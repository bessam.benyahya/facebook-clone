import { initializeApp, getApps, getApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getStorage } from 'firebase/storage'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyCmQqnChiMA0QmGsy_-VYG7BBQE1IGz4W0',
  authDomain: 'facebook-clone-a436d.firebaseapp.com',
  projectId: 'facebook-clone-a436d',
  storageBucket: 'facebook-clone-a436d.appspot.com',
  messagingSenderId: '430690108177',
  appId: '1:430690108177:web:071b62eda88feb01c842b0',
}

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp()
const db = getFirestore()
const storage = getStorage()

export { app, db, storage }
